import * as React from "react";
import {View, FlatList} from "react-native";
import {COLORS} from "../../assets/styles";
import {mockHomeData} from "../../assets/data";
import ItemHome from "./ItemHome";
import Header from "../../components/Header";

class Home extends React.Component {

    static navigationOptions = ({navigation}: any) => {
        return {
            headerTitle: <Header navigation={navigation}/>
        }
    };

    _handlePressItem = (id: number) => {
        console.log(id)
    };

    _renderItem = ({item, index}: any) => {
        const {id, name, message, location, value, time} = item;
        return (
            <ItemHome key={index}
                      id={id} name={name}
                      message={message}
                      location={location}
                      value={value}
                      time={time}
                      onPress={this._handlePressItem}/>
        )
    };

    _renderSeparator = () => (
        <View
            style={{
                backgroundColor: COLORS.separatorColor,
                height: 0.5,
            }}
        />
    );

    _keyExtractor = (item: any, index: number) => {
        return item.name + index
    };

    render() {
        return (
            <FlatList
                style={{flex: 1, backgroundColor: COLORS.white}}
                data={mockHomeData}
                keyExtractor={this._keyExtractor}
                removeClippedSubviews={true}
                ItemSeparatorComponent={this._renderSeparator}
                renderItem={this._renderItem}/>
        );
    }
}

export default Home