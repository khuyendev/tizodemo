import React from "react";
import {View, Text, TouchableOpacity, StyleSheet} from "react-native";
import {COLORS, PADDING, TEXT_SIZE} from "../../assets/styles";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import {numberFormat} from "../../utilities/helper";

interface Props {
    id: number
    name: string
    message: string
    location: string
    value: number
    time: string

    onPress(id: number): void
}

class ItemHome extends React.PureComponent<Props> {

    _handlePress = () => {
        const {id} = this.props;
        this.props.onPress && this.props.onPress(id)
    }

    render() {
        const {name, message, location, value, time} = this.props;
        return (
            <TouchableOpacity
                onPress={this._handlePress}
                style={styles.container}>
                <View style={styles.container_title}>
                    <Text style={styles.name}>{name}</Text>
                    <Text style={styles.time}>{time}</Text>
                    <Icon name={"chevron-right"}
                          size={20}
                          color={COLORS.grayIcon}
                    />
                </View>
                <Text style={styles.message}>{message}</Text>
                <Text style={styles.location}>{location}</Text>
                <Text style={styles.value}>{numberFormat(value)}</Text>
            </TouchableOpacity>

        );
    }
}

const styles = StyleSheet.create({
    container: {flex: 1, padding: PADDING.defaultPadding},
    container_title: {
        flexDirection: "row",
        paddingBottom: PADDING.defaultPadding / 2
    },
    name: {flex: 1, fontSize: TEXT_SIZE.big, color: COLORS.black},
    time: {color: COLORS.grayText, fontSize: TEXT_SIZE.big},
    message: {color: COLORS.grayText, fontSize: TEXT_SIZE.normal},
    location: {color: COLORS.grayText, fontSize: TEXT_SIZE.small},
    value: {color: COLORS.black, paddingTop: PADDING.defaultPadding / 2}
})
export default ItemHome