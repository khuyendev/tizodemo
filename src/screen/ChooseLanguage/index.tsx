import React from "react";
import {View, Text, Image, SafeAreaView, TouchableOpacity, StyleSheet} from "react-native";
import {COLORS, PADDING, TEXT_SIZE} from "../../assets/styles";
import LinearGradient from "react-native-linear-gradient";
import {DEFAULT_LANG, LanguageSupported} from "../../config/app";
import ItemLanguage from "./ItemLanguage";
import i18n, {switchLanguage} from "../../utilities/LangHelper"
import {saveToStorage} from "../../utilities/helper";
import Constants from "../../config/constants";
import NavigationService from "../../services/NavigationService";
import {HomeRoute} from "../../config/routes";
import Images from "../../assets/Images";

interface State {
    languageSelected: string
}

class ChooseLanguage extends React.Component<any, State> {
    constructor(props: any) {
        super(props);
        this.state = {
            languageSelected: DEFAULT_LANG
        }
    }

    _handleItemChecked = (symbol: string) => {
        if (symbol === this.state.languageSelected) {
            return;
        }
        switchLanguage(symbol).then(() => {
            this.setState({
                languageSelected: symbol
            }, () => {
                this.forceUpdate()
            });

        }).catch(() => {
            alert("Somethings went wrong...")
        })
    };

    _handleNextPress = () => {
        saveToStorage(Constants.STORE_LANGUAGE_KEY, Constants.STORE_LANGUAGE_SELECTED).then(() => {
            NavigationService.navigate(HomeRoute.HOME)
        })
    };

    render() {
        const {languageSelected} = this.state;
        return (
            <SafeAreaView style={{flex: 1}}>
                <View style={styles.container}>
                    <View style={{alignItems: "center"}}>
                        <Image style={styles.logo} resizeMode={"contain"} source={Images.ic_logo}/>
                        <Text style={styles.description}>{i18n.t("choose_language.description")}</Text>
                    </View>
                    <View style={{paddingHorizontal: PADDING.defaultPadding}}>
                        <Text style={styles.text_choose_lan}>{i18n.t("choose_language.your_languages")}</Text>
                        {LanguageSupported.map((item, index) => {
                            return (<ItemLanguage key={index}
                                                  name={item.name}
                                                  icon={item.icon}
                                                  selected={languageSelected}
                                                  onChecked={this._handleItemChecked}
                                                  symbol={item.symbol}/>)
                        })}
                    </View>
                    <TouchableOpacity style={styles.btn_next_container}
                                      onPress={this._handleNextPress}
                    >
                        <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                        colors={[COLORS.buttonGradientStart, COLORS.buttonGradientEnd]}
                                        style={styles.gradient}>
                            <Text style={styles.btn_next_text}>{i18n.t("choose_language.btn_next")}</Text>
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: PADDING.defaultPadding,
        paddingVertical: PADDING.defaultPadding * 2,
        alignItems: "center",
        justifyContent: "space-between"
    },
    logo: {
        width: 150, height: 100
    },
    description: {
        color: COLORS.black,
        fontSize: TEXT_SIZE.big,
        textAlign: "center"
    },
    text_choose_lan: {
        color: "#A5A8B0",
        fontSize: TEXT_SIZE.normal
    },
    btn_next_container: {
        width: "100%", height: 48
    },
    gradient: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 5
    },
    btn_next_text: {
        color: COLORS.white,
        fontSize: TEXT_SIZE.normal
    }
});
export default ChooseLanguage