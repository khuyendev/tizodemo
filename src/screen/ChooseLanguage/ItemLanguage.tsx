import React from "react";
import {View, Image, Text, TouchableOpacity, StyleSheet} from "react-native";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {COLORS, PADDING} from "../../assets/styles";

interface Props {
    name: string
    selected: string
    symbol: string
    icon: any

    onChecked(symbol: string): void
}

class ItemLanguage extends React.PureComponent<Props> {

    _handlePress = () => {
        this.props.onChecked && this.props.onChecked(this.props.symbol)
    }

    render(): React.ReactNode {
        const {name, selected, symbol, icon} = this.props;
        const shouldRenderChked = selected && selected === symbol;
        return (
            <TouchableOpacity style={{width: "100%", paddingVertical: PADDING.defaultPadding / 2}}
                              disabled={!!shouldRenderChked}
                              onPress={this._handlePress}
            >
                <View style={styles.container}>
                    <Image style={styles.flag}
                           source={icon}/>
                    <Text style={styles.name}>{name}</Text>
                    {shouldRenderChked ? < Icon size={24} name="check" color={COLORS.primaryColor}/> : null}
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between"
    },
    flag: {
        width: 48,
        height: 48
    },
    name: {
        flex: 1,
        paddingHorizontal: PADDING.defaultPadding,
        color: COLORS.black
    }
})
export default ItemLanguage;