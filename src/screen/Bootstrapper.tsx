import React from "react";
import {ActivityIndicator, StatusBar, View} from "react-native";
import {COLORS} from "../assets/styles";
import {getFromStorage} from "../utilities/helper";
import Constants from "../config/constants";
import {AppRoute} from "../config/routes";
import NavigationService from "../services/NavigationService"

interface Props {
    navigation: any
    account: any
}

export default class Bootstrapper extends React.Component<Props> {
    constructor(props: Props) {
        super(props);
        this._bootstrap();
    }

    _bootstrap = () => {
        getFromStorage(Constants.STORE_LANGUAGE_KEY).then((value: any) => {
            // if (value === Constants.STORE_LANGUAGE_SELECTED) {
            //     NavigationService.navigate(AppRoute.HOME_ROUTE)
            // } else {
            NavigationService.navigate(AppRoute.CHOOSE_LANG_ROUTE)
            // }
        }).catch((e: any) => {
            NavigationService.navigate(AppRoute.CHOOSE_LANG_ROUTE)
        })
    };

    render() {
        return (
            <View style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
            }}>
                <ActivityIndicator color={COLORS.primaryColor}/>
                <StatusBar barStyle="default"/>
            </View>
        );
    }
}