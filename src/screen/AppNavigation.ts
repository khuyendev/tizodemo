import {createAppContainer, createDrawerNavigator, createStackNavigator, createSwitchNavigator} from "react-navigation";
import {AppRoute, HomeRoute} from "../config/routes";
import ChooseLanguage from "./ChooseLanguage";
import Bootstrapper from "./Bootstrapper";
import Home from "./Home";
import DrawerComponent from "../components/Drawer";
import {EmptyScreen} from "./EmptyScreen";

const HomeStack = createStackNavigator({
    [HomeRoute.HOME]: {
        screen: Home,
    },
    [HomeRoute.EMPTY]: {
        screen: EmptyScreen,
    },
});

HomeStack.navigationOptions = ({navigation}: any) => {
    let drawerLockMode = "unlocked";
    if (navigation.state.index > 0) {
        drawerLockMode = "locked-closed";
    }
    return {
        drawerLockMode,
    };
};

const HomeDrawerNavigator = createDrawerNavigator({
    [HomeRoute.DRAWER]: HomeStack
}, {
    contentComponent: DrawerComponent
});

const AppSwitch = createSwitchNavigator({
    [AppRoute.BOOSTRAP_SCREEN]: Bootstrapper,
    [AppRoute.CHOOSE_LANG_ROUTE]: ChooseLanguage,
    [AppRoute.HOME_ROUTE]: HomeDrawerNavigator,
}, {
    initialRouteName: AppRoute.BOOSTRAP_SCREEN,
});

const AppContainer = createAppContainer(AppSwitch);
export default AppContainer;
