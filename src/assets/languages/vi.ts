export default {
    choose_language: {
        description: "Kiểm tra giá của bạn và\nkhông bao giờ trả quá nhiều",
        your_languages: "Ngôn ngữ của bạn",
        btn_next: "Tiếp tục"
    },
    home: {
        header_title: "Trò chuyện với khách hàng tiềm năng",
    },
    item_drawer: {
        your_loan: "Khoản vay của bạn",
        notifications: "Thông báo",
        profile: "Hồ sơ",
        settings: "Cài đặt",
    }
}