export default {
    choose_language: {
        description: "Check your rates and\nnever overpay again",
        your_languages: "Your languages",
        btn_next: "Next"
    },
    home: {
        header_title: "Chat with a potential client",
    },
    item_drawer: {
        your_loan: "Your Loans",
        notifications: "Notifications",
        profile: "Profile",
        settings: "Settings",
    }
}