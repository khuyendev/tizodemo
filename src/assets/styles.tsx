const COLORS = {
    primaryColor: "#6461AA",
    itemSeparator: "#C0C0C0",
    buttonGradientStart: "#5069B0",
    buttonGradientEnd: "#785DA8",
    white: "#FFF",
    black: "#000",
    grayIcon: "#BEC0C6",
    grayText: "#525659",
    separatorColor: "#BEC0C6",
};
const PADDING = {
    defaultPadding: 16,
};

const TEXT_SIZE = {
    headerTitle: 18,
    small: 12,
    normal: 14,
    big: 16
}

export {
    COLORS,
    PADDING,
    TEXT_SIZE
}