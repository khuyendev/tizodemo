const Constants = {
    STORE_LANGUAGE_KEY: "@STORE_LANGUAGE_KEY",
    STORE_LANGUAGE_SELECTED: "@STORE_LANGUAGE_SELECTED",
    STORE_LANGUAGE_NOT_SELECTED: "@STORE_LANGUAGE_NOT_SELECTED",
}
export default Constants