const AppRoute = {
    BOOSTRAP_SCREEN: "BOOSTRAP_SCREEN",
    CHOOSE_LANG_ROUTE: "CHOOSE_LANG_ROUTE",
    HOME_ROUTE: "HOME_ROUTE",
};

const HomeRoute = {
    DRAWER: "DRAWER",
    HOME: "HOME",
    EMPTY: "EMPTY",
};

export {
    AppRoute,
    HomeRoute
}