import Images from "../assets/Images";

export const LanguageSupported = [
    {
        name: "English",
        symbol: "en",
        icon: Images.ic_flag_us
    },
    {
        name: "Tiếng Việt",
        symbol: "vi",
        icon: Images.ic_flag_vn
    }
];
export const DEFAULT_LANG = "en";
export const FALLBACK = "vi";