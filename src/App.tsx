import React, {Component} from 'react';
import AppContainer from "./screen/AppNavigation";
import NavigationService from "./services/NavigationService";


export default class App extends Component {

    render() {
        return (
            <AppContainer
                ref={(navigatorRef: any) => {
                    NavigationService.setTopLevelNavigator(navigatorRef);
                }}
            />
        );
    }

}