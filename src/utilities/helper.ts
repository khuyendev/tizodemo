import AsyncStorage from "@react-native-community/async-storage";
import Images from "../assets/Images";

export async function saveToStorage(key: string, value: string) {
    return await AsyncStorage.setItem(key, value);
}

export async function getFromStorage(key: string) {
    return await AsyncStorage.getItem(key);
}

export function numberFormat(number:any) {
    const pieces = parseFloat(number).toFixed(0).split("")
    let ii = pieces.length;
    while ((ii-=3) > 0) {
        pieces.splice(ii, 0, ",")
    }
    return  pieces.join("")
}