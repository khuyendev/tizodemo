import i18next from 'i18next';
import vi from "../assets/languages/vi";
import en from "../assets/languages/en";
import {DEFAULT_LANG, FALLBACK} from "../config/app";

i18next.init({
    interpolation: {
        escapeValue: false,
    },
    lng: DEFAULT_LANG,
    fallbackLng: FALLBACK,
    resources: {
        vi: {
            translation: vi
        },
        en: {
            translation: en
        }
    },
});

export async function switchLanguage(lang: string) {
    return await i18next.changeLanguage(lang)
}
export default i18next;
