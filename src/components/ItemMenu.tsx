import React from "react";
import {Text, TouchableOpacity, StyleSheet} from "react-native";
import {COLORS, PADDING, TEXT_SIZE} from "../assets/styles";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

interface Props {
    id: number
    name: string
    icon: string

    onPress(id: number): void
}

class ItemMenu extends React.PureComponent<Props> {

    _handlePress = () => {
        const {id} = this.props;
        this.props.onPress && this.props.onPress(id)
    }

    render() {
        const {name, icon} = this.props;
        return (
            <TouchableOpacity
                onPress={this._handlePress}
                style={styles.container}>
                <Icon name={icon}
                      size={24}
                      color={COLORS.grayIcon}
                />
                <Text style={styles.name}>{name}</Text>
            </TouchableOpacity>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: 48,
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: PADDING.defaultPadding,
        paddingVertical: PADDING.defaultPadding / 2
    },
    name: {
        paddingHorizontal: PADDING.defaultPadding * 2,
        color: COLORS.black,
    },
});

export default ItemMenu