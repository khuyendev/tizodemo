import React from "react";
import {DrawerActions} from "react-navigation";
import LinearGradient from "react-native-linear-gradient";
import {COLORS, PADDING, TEXT_SIZE} from "../assets/styles";
import {StyleSheet, Text, TouchableOpacity, View} from "react-native";
import i18n from "../utilities/LangHelper";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

interface Props {
    navigation: any
}

class Header extends React.Component<Props> {

    _handleMenuPress = () => {
        this.props.navigation && this.props.navigation.dispatch(DrawerActions.toggleDrawer());
    }

    render() {
        return (
            <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                            colors={[COLORS.buttonGradientStart, COLORS.buttonGradientEnd]}
                            style={{
                                flex: 1,
                            }}>
                <View style={styles.container}>
                    <TouchableOpacity onPress={this._handleMenuPress}>
                        <Icon name={"menu"}
                              style={{padding: PADDING.defaultPadding / 2}}
                              size={24}
                              color={COLORS.white}
                        />
                    </TouchableOpacity>
                    <Text
                        style={styles.title}>{i18n.t("home.header_title")}</Text>
                </View>
            </LinearGradient>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: 56,
        flexDirection: "row",
        alignItems: "center",
    },
    title: {
        color: COLORS.white,
        fontSize: TEXT_SIZE.headerTitle,
        fontWeight: "bold",
        paddingHorizontal: PADDING.defaultPadding / 2
    },
});

export default Header