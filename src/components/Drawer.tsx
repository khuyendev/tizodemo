import React from "react";
import {SafeAreaView, View, Text, StyleSheet, Image} from "react-native";
import {DrawerItemsProps} from "react-navigation";
import ItemMenu from "./ItemMenu";
import Images from "../assets/Images";
import {COLORS, PADDING} from "../assets/styles";
import i18n from "../utilities/LangHelper";
import NavigationService from "../services/NavigationService";
import {HomeRoute} from "../config/routes";

class DrawerComponent extends React.PureComponent<DrawerItemsProps> {

    _handleItemPress = (id: number) => {
//navigate by id here
        NavigationService.navigate(HomeRoute.EMPTY)
    }

    render() {
        const ItemMenus = [
            {
                name: i18n.t("item_drawer.your_loan"),
                icon: "database",
            },
            {
                name: i18n.t("item_drawer.notifications"),
                icon: "bell-ring",
            },
            {
                name: i18n.t("item_drawer.profile"),
                icon: "account",
            },
            {
                name: i18n.t("item_drawer.settings"),
                icon: "settings",
            }
        ]
        return (
            <SafeAreaView style={{flex: 1}}>
                <View style={styles.container}>
                    <Image style={styles.logo} resizeMode={"contain"} source={Images.ic_logo}/>
                    <Text style={styles.textVersion}>v1.0</Text>
                </View>
                {ItemMenus.map((item, index) => {
                    return (
                        <ItemMenu
                            id={index}
                            key={index}
                            onPress={this._handleItemPress}
                            name={item.name}
                            icon={item.icon}/>)
                })}

            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "flex-end",
        padding: PADDING.defaultPadding
    },
    logo: {
        width: 100, height: 50
    },
    textVersion: {color: COLORS.grayText}
});
export default DrawerComponent